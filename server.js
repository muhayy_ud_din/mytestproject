
var express = require('express');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var cors = require('cors');
var https = require('https');
var http = require('http');

var fs = require('fs');

process.env.APPINSIGHTS_INSTRUMENTATIONKEY = process.env.APPINSIGHTS_INSTRUMENTATIONKEY || "NO_APPLICATION_INSIGHTS";
appInsights = require("applicationinsights");
appInsights.setup().setAutoCollectExceptions(true)

var clientCounter = 1;
var clientToId = {};
var peers = {};
var connectionsToClean = new Set();

var port = process.env.PORT || 3000;
var allowedOrigins = (process.env.CORS_ORIGINS || '*').split(',')
var intervalToCleanConnections = process.env.INTERVAL || 10000;
var privateKey = fs.readFileSync('key.pem', 'utf8');
var certificate = fs.readFileSync('server.crt', 'utf8');

var credentials = { key: privateKey, cert: certificate };

var app = express();

//var httpsServer = https.createServer(credentials,app);
var httpsServer = http.createServer(app);

httpsServer.keepAliveTimeout = 120000;

app.use(helmet())
app.use(cors({
    origin: (origin, cb) => cb(null, allowedOrigins.indexOf('*') !== -1 || allowedOrigins.indexOf(origin) !== -1)
}))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.text())
//app.use(express.json());
app.use(ignoreFavicon);

function ignoreFavicon(req, res, next) {
    if (req.originalUrl === '/favicon.ico') {
      res.status(204).json({nope: true});
    } else {
      next();
    }
  }

app.all('*', function (req, res, next) {
    //log(req.url);
    if (req.query.peer_id && peers[req.query.peer_id]) {
        peers[req.query.peer_id].lastSeenActive = (new Date()).getTime();
    }
    next();
});

function cleanPeerList() {
    for (peerId in peers) {
        var peer = peers[peerId]
        if (peer.lastSeenActive + intervalToCleanConnections < new Date()) {
            log("Deleting peer " + peerId);
            if (peer.roomPeer) {
                log("Peer " + peerId + " crashed. Making " + peer.roomPeer.id + " available")
                peer.roomPeer.roomPeer = null;
            }
            delete peers[peerId];
            delete peers[peerId];
        }
    }
}

setInterval(cleanPeerList, intervalToCleanConnections);


app.get('/sign_in', function (req, res) {
    var client = {};
    //log(req.url);
    var newPeer = {}
    newPeer.id = clientCounter++;
    newPeer.peerType = 'client';
    newPeer.messages = [];

    newPeer.name = req.query.peer_name;
    if (newPeer.name.indexOf("renderingclient_") != -1) {
        newPeer.peerType = 'client';
    }
    if (newPeer.name.indexOf("renderingserver_") != -1) {
        newPeer.peerType = 'server';
    }
    newPeer.timestampOfLastHeartbeat = (new Date()).getTime();
    peers[newPeer.id] = newPeer;

    for(peer in peers)
    {
        log(peers[peer].name+ "  ID is  "+peers[peer].id);
    }
    res.set('Pragma', newPeer.id);
    
    res.send(formatListOfPeers(newPeer))
    notifyOtherPeers(newPeer);
})

app.post('/message', function (req, res) {
    log("The Received URL REQUEST IS  "+req.url);
    
    log("Body payload is "+ req.body);
    var fromId = req.query.peer_id;
    var toId = req.query.to;
    var payload = req.body;
    var contentLength = req.headers['content-length'];
    contentLength = parseInt(contentLength);
    if (!peers[toId] || !peers[fromId]) {
        res.status(400).send();
    }
    if (contentLength <= payload.length) {
       // peers[toId].roomPeer = peers[fromId];
       // peers[fromId].roomPeer = peers[toId];

        sendMessageToPeer(peers[toId], payload, fromId);

        res.set('Pragma', fromId);
        res.send("");
    }
})

app.get('/heartbeat', function (req, res) {
    res.sendStatus(200);
})
app.get('/', function (req, res) {
    res.sendStatus(200);
})

app.get('/sign_out', function (req, res) {
    log(req.url);
    var peerId = req.query.peer_id;

    var peer = peers[peerId];

   if (peer.roomPeer) {
        peer.roomPeer.roomPeer = null;
   }
  

    delete peers[peerId];
    notifyOtherPeersAboutDeletedPeer(peer);
    res.set('Pragma', peerId);
    res.send("Ok");
})

app.get('/wait', function (req, res) {
    log(req.url);
    var peerId = req.query.peer_id;

    
   // if (connectionsToClean.has(peerId)) {
    //    connectionsToClean.delete(peerId)
    //}

    if (peers[peerId]) {
        var socket = {};
        socket.waitPeer = peers[peerId];
        socket.res = res;
        peers[peerId].waitSocket = socket;

        sendMessageToPeer(peers[peerId], null, null);
    }

    /*req.on('close', function () {
      connectionsToClean.add(peerId);
       setTimeout(function () {
        connectionsToClean.forEach(function (peerId) {
                if (peers[peerId]) {
                    if (peers[peerId].roomPeer) {
                        log("Peer " + peerId + " crashed. Making " + peers[peerId].roomPeer.id + " available")
                       peers[peerId].roomPeer.roomPeer = null;
                    }
                    log("Connection close. Deleteting peer " + peerId);
                    delete peers[peerId];
                }
            });
            connectionsToClean = new Set();
        }, 3000);
    });*/

})



function formatListOfPeers(peer) {
    var result = peer.name + "," + peer.id + ",1\n";
    for (peerId in peers) {
        var otherPeer = peers[peerId];
        if (isPeerCandidate(peer, otherPeer)) {
            result += otherPeer.name + "," + otherPeer.id + ",1\n"
        }
    }
    return result;
}

var logCounter = 0;
function log(message) {
    console.log(logCounter++ + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + " " + message);
    appInsights.getClient().trackTrace(logCounter + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + " " + message);
}

function notifyOtherPeers(newPeer) {
    for (peerId in peers) {
        var otherPeer = peers[peerId];
        if (isPeerCandidate(newPeer, otherPeer)) {
            var data = newPeer.name + "," + newPeer.id + ",1\n";
            sendMessageToPeer(otherPeer, data);
        }
    }
}

function notifyOtherPeersAboutDeletedPeer(deletedPeer) {
    for (peerId in peers) {
        var otherPeer = peers[peerId];
            var data = deletedPeer.name + "," + deletedPeer.id + ",0\n";
            sendMessageToPeer(otherPeer, data);
        
    }
}

function sendMessageToPeer(peer, payload, fromId) {
    var msg = {};
    if (payload) {
        msg.id = fromId || peer.id;
        msg.payload = payload;
        peer.messages.push(msg);
        
    }
    if (peer.waitSocket) {
        msg = peer.messages.shift();
        if (msg) {
            peer.waitSocket.res.set('Pragma', msg.id);
            peer.waitSocket.res.send(msg.payload);
            peer.waitSocket.waitPeer = null;
            peer.waitSocket.tmpData = "";
            peer.waitSocket = null;
        }
    }
}

function isPeerCandidate(peer, otherPeer) {
    return (otherPeer.id != peer.id);// && // filter self
        //!otherPeer.roomPeer && // filter peers in 'rooms'
        //otherPeer.peerType != peer.peerType) // filter out peers of same type
}

log("Signaling server running at port " + port);

const server = httpsServer.listen(port);
module.exports = server;