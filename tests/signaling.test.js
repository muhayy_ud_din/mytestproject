const request = require('supertest')
let server 

describe('GET Requests', () =>{

beforeEach(()=> { server = require('../server'); })
afterEach(()=> { server.close();})

it("should connect to the server and return response 200", async () =>{

    const res = await request(server).get('/')
    expect(res.status).toBe(200);

});

it("should sign_in new users", async () =>{

    const res = await request(server).get('/sign_in?peer_name=dummyUsr1')
    const res1 = await request(server).get('/sign_in?peer_name=dummyUsr2')      
    expect(res.text.split(',')[0]).toBe('dummyUsr1');
    expect(res1.text.split(',')[0]).toBe('dummyUsr2');

});

it("should sign_out connect users", async () =>{

    const res = await request(server).get('/sign_out?peer_id=1')
    const res1 = await request(server).get('/sign_out?peer_id=2') 
    expect(res.status).toBe(200);
    expect(res1.status).toBe(200);

});

});